// Code generated by protoc-gen-validate. DO NOT EDIT.
// source: permission_svc.proto

package appcontextsvc_client

import (
	"bytes"
	"errors"
	"fmt"
	"net"
	"net/mail"
	"net/url"
	"regexp"
	"sort"
	"strings"
	"time"
	"unicode/utf8"

	"google.golang.org/protobuf/types/known/anypb"
)

// ensure the imports are used
var (
	_ = bytes.MinRead
	_ = errors.New("")
	_ = fmt.Print
	_ = utf8.UTFMax
	_ = (*regexp.Regexp)(nil)
	_ = (*strings.Reader)(nil)
	_ = net.IPv4len
	_ = time.Duration(0)
	_ = (*url.URL)(nil)
	_ = (*mail.Address)(nil)
	_ = anypb.Any{}
	_ = sort.Sort
)

// define the regex for a UUID once up-front
var _permission_svc_uuidPattern = regexp.MustCompile("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$")

// Validate checks the field values on ListPermissionsResponse with the rules
// defined in the proto definition for this message. If any rules are
// violated, the first error encountered is returned, or nil if there are no violations.
func (m *ListPermissionsResponse) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on ListPermissionsResponse with the
// rules defined in the proto definition for this message. If any rules are
// violated, the result is a list of violation errors wrapped in
// ListPermissionsResponseMultiError, or nil if none found.
func (m *ListPermissionsResponse) ValidateAll() error {
	return m.validate(true)
}

func (m *ListPermissionsResponse) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	for idx, item := range m.GetPermissions() {
		_, _ = idx, item

		if all {
			switch v := interface{}(item).(type) {
			case interface{ ValidateAll() error }:
				if err := v.ValidateAll(); err != nil {
					errors = append(errors, ListPermissionsResponseValidationError{
						field:  fmt.Sprintf("Permissions[%v]", idx),
						reason: "embedded message failed validation",
						cause:  err,
					})
				}
			case interface{ Validate() error }:
				if err := v.Validate(); err != nil {
					errors = append(errors, ListPermissionsResponseValidationError{
						field:  fmt.Sprintf("Permissions[%v]", idx),
						reason: "embedded message failed validation",
						cause:  err,
					})
				}
			}
		} else if v, ok := interface{}(item).(interface{ Validate() error }); ok {
			if err := v.Validate(); err != nil {
				return ListPermissionsResponseValidationError{
					field:  fmt.Sprintf("Permissions[%v]", idx),
					reason: "embedded message failed validation",
					cause:  err,
				}
			}
		}

	}

	// no validation rules for PageNumber

	// no validation rules for PageSize

	// no validation rules for TotalCount

	if len(errors) > 0 {
		return ListPermissionsResponseMultiError(errors)
	}

	return nil
}

// ListPermissionsResponseMultiError is an error wrapping multiple validation
// errors returned by ListPermissionsResponse.ValidateAll() if the designated
// constraints aren't met.
type ListPermissionsResponseMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m ListPermissionsResponseMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m ListPermissionsResponseMultiError) AllErrors() []error { return m }

// ListPermissionsResponseValidationError is the validation error returned by
// ListPermissionsResponse.Validate if the designated constraints aren't met.
type ListPermissionsResponseValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e ListPermissionsResponseValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e ListPermissionsResponseValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e ListPermissionsResponseValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e ListPermissionsResponseValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e ListPermissionsResponseValidationError) ErrorName() string {
	return "ListPermissionsResponseValidationError"
}

// Error satisfies the builtin error interface
func (e ListPermissionsResponseValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sListPermissionsResponse.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = ListPermissionsResponseValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = ListPermissionsResponseValidationError{}

// Validate checks the field values on GetPermissionRequest with the rules
// defined in the proto definition for this message. If any rules are
// violated, the first error encountered is returned, or nil if there are no violations.
func (m *GetPermissionRequest) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on GetPermissionRequest with the rules
// defined in the proto definition for this message. If any rules are
// violated, the result is a list of violation errors wrapped in
// GetPermissionRequestMultiError, or nil if none found.
func (m *GetPermissionRequest) ValidateAll() error {
	return m.validate(true)
}

func (m *GetPermissionRequest) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	if m.GetId() != "" {

		if err := m._validateUuid(m.GetId()); err != nil {
			err = GetPermissionRequestValidationError{
				field:  "Id",
				reason: "value must be a valid UUID",
				cause:  err,
			}
			if !all {
				return err
			}
			errors = append(errors, err)
		}

	}

	if len(errors) > 0 {
		return GetPermissionRequestMultiError(errors)
	}

	return nil
}

func (m *GetPermissionRequest) _validateUuid(uuid string) error {
	if matched := _permission_svc_uuidPattern.MatchString(uuid); !matched {
		return errors.New("invalid uuid format")
	}

	return nil
}

// GetPermissionRequestMultiError is an error wrapping multiple validation
// errors returned by GetPermissionRequest.ValidateAll() if the designated
// constraints aren't met.
type GetPermissionRequestMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m GetPermissionRequestMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m GetPermissionRequestMultiError) AllErrors() []error { return m }

// GetPermissionRequestValidationError is the validation error returned by
// GetPermissionRequest.Validate if the designated constraints aren't met.
type GetPermissionRequestValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e GetPermissionRequestValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e GetPermissionRequestValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e GetPermissionRequestValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e GetPermissionRequestValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e GetPermissionRequestValidationError) ErrorName() string {
	return "GetPermissionRequestValidationError"
}

// Error satisfies the builtin error interface
func (e GetPermissionRequestValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sGetPermissionRequest.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = GetPermissionRequestValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = GetPermissionRequestValidationError{}

// Validate checks the field values on CreatePermissionRequest with the rules
// defined in the proto definition for this message. If any rules are
// violated, the first error encountered is returned, or nil if there are no violations.
func (m *CreatePermissionRequest) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on CreatePermissionRequest with the
// rules defined in the proto definition for this message. If any rules are
// violated, the result is a list of violation errors wrapped in
// CreatePermissionRequestMultiError, or nil if none found.
func (m *CreatePermissionRequest) ValidateAll() error {
	return m.validate(true)
}

func (m *CreatePermissionRequest) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	if all {
		switch v := interface{}(m.GetPermission()).(type) {
		case interface{ ValidateAll() error }:
			if err := v.ValidateAll(); err != nil {
				errors = append(errors, CreatePermissionRequestValidationError{
					field:  "Permission",
					reason: "embedded message failed validation",
					cause:  err,
				})
			}
		case interface{ Validate() error }:
			if err := v.Validate(); err != nil {
				errors = append(errors, CreatePermissionRequestValidationError{
					field:  "Permission",
					reason: "embedded message failed validation",
					cause:  err,
				})
			}
		}
	} else if v, ok := interface{}(m.GetPermission()).(interface{ Validate() error }); ok {
		if err := v.Validate(); err != nil {
			return CreatePermissionRequestValidationError{
				field:  "Permission",
				reason: "embedded message failed validation",
				cause:  err,
			}
		}
	}

	if len(errors) > 0 {
		return CreatePermissionRequestMultiError(errors)
	}

	return nil
}

// CreatePermissionRequestMultiError is an error wrapping multiple validation
// errors returned by CreatePermissionRequest.ValidateAll() if the designated
// constraints aren't met.
type CreatePermissionRequestMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m CreatePermissionRequestMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m CreatePermissionRequestMultiError) AllErrors() []error { return m }

// CreatePermissionRequestValidationError is the validation error returned by
// CreatePermissionRequest.Validate if the designated constraints aren't met.
type CreatePermissionRequestValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e CreatePermissionRequestValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e CreatePermissionRequestValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e CreatePermissionRequestValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e CreatePermissionRequestValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e CreatePermissionRequestValidationError) ErrorName() string {
	return "CreatePermissionRequestValidationError"
}

// Error satisfies the builtin error interface
func (e CreatePermissionRequestValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sCreatePermissionRequest.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = CreatePermissionRequestValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = CreatePermissionRequestValidationError{}

// Validate checks the field values on UpdatePermissionRequest with the rules
// defined in the proto definition for this message. If any rules are
// violated, the first error encountered is returned, or nil if there are no violations.
func (m *UpdatePermissionRequest) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on UpdatePermissionRequest with the
// rules defined in the proto definition for this message. If any rules are
// violated, the result is a list of violation errors wrapped in
// UpdatePermissionRequestMultiError, or nil if none found.
func (m *UpdatePermissionRequest) ValidateAll() error {
	return m.validate(true)
}

func (m *UpdatePermissionRequest) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	if all {
		switch v := interface{}(m.GetPermission()).(type) {
		case interface{ ValidateAll() error }:
			if err := v.ValidateAll(); err != nil {
				errors = append(errors, UpdatePermissionRequestValidationError{
					field:  "Permission",
					reason: "embedded message failed validation",
					cause:  err,
				})
			}
		case interface{ Validate() error }:
			if err := v.Validate(); err != nil {
				errors = append(errors, UpdatePermissionRequestValidationError{
					field:  "Permission",
					reason: "embedded message failed validation",
					cause:  err,
				})
			}
		}
	} else if v, ok := interface{}(m.GetPermission()).(interface{ Validate() error }); ok {
		if err := v.Validate(); err != nil {
			return UpdatePermissionRequestValidationError{
				field:  "Permission",
				reason: "embedded message failed validation",
				cause:  err,
			}
		}
	}

	if all {
		switch v := interface{}(m.GetUpdateMask()).(type) {
		case interface{ ValidateAll() error }:
			if err := v.ValidateAll(); err != nil {
				errors = append(errors, UpdatePermissionRequestValidationError{
					field:  "UpdateMask",
					reason: "embedded message failed validation",
					cause:  err,
				})
			}
		case interface{ Validate() error }:
			if err := v.Validate(); err != nil {
				errors = append(errors, UpdatePermissionRequestValidationError{
					field:  "UpdateMask",
					reason: "embedded message failed validation",
					cause:  err,
				})
			}
		}
	} else if v, ok := interface{}(m.GetUpdateMask()).(interface{ Validate() error }); ok {
		if err := v.Validate(); err != nil {
			return UpdatePermissionRequestValidationError{
				field:  "UpdateMask",
				reason: "embedded message failed validation",
				cause:  err,
			}
		}
	}

	if len(errors) > 0 {
		return UpdatePermissionRequestMultiError(errors)
	}

	return nil
}

// UpdatePermissionRequestMultiError is an error wrapping multiple validation
// errors returned by UpdatePermissionRequest.ValidateAll() if the designated
// constraints aren't met.
type UpdatePermissionRequestMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m UpdatePermissionRequestMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m UpdatePermissionRequestMultiError) AllErrors() []error { return m }

// UpdatePermissionRequestValidationError is the validation error returned by
// UpdatePermissionRequest.Validate if the designated constraints aren't met.
type UpdatePermissionRequestValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e UpdatePermissionRequestValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e UpdatePermissionRequestValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e UpdatePermissionRequestValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e UpdatePermissionRequestValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e UpdatePermissionRequestValidationError) ErrorName() string {
	return "UpdatePermissionRequestValidationError"
}

// Error satisfies the builtin error interface
func (e UpdatePermissionRequestValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sUpdatePermissionRequest.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = UpdatePermissionRequestValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = UpdatePermissionRequestValidationError{}

// Validate checks the field values on DeletePermissionRequest with the rules
// defined in the proto definition for this message. If any rules are
// violated, the first error encountered is returned, or nil if there are no violations.
func (m *DeletePermissionRequest) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on DeletePermissionRequest with the
// rules defined in the proto definition for this message. If any rules are
// violated, the result is a list of violation errors wrapped in
// DeletePermissionRequestMultiError, or nil if none found.
func (m *DeletePermissionRequest) ValidateAll() error {
	return m.validate(true)
}

func (m *DeletePermissionRequest) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	if m.GetId() != "" {

		if err := m._validateUuid(m.GetId()); err != nil {
			err = DeletePermissionRequestValidationError{
				field:  "Id",
				reason: "value must be a valid UUID",
				cause:  err,
			}
			if !all {
				return err
			}
			errors = append(errors, err)
		}

	}

	if len(errors) > 0 {
		return DeletePermissionRequestMultiError(errors)
	}

	return nil
}

func (m *DeletePermissionRequest) _validateUuid(uuid string) error {
	if matched := _permission_svc_uuidPattern.MatchString(uuid); !matched {
		return errors.New("invalid uuid format")
	}

	return nil
}

// DeletePermissionRequestMultiError is an error wrapping multiple validation
// errors returned by DeletePermissionRequest.ValidateAll() if the designated
// constraints aren't met.
type DeletePermissionRequestMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m DeletePermissionRequestMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m DeletePermissionRequestMultiError) AllErrors() []error { return m }

// DeletePermissionRequestValidationError is the validation error returned by
// DeletePermissionRequest.Validate if the designated constraints aren't met.
type DeletePermissionRequestValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e DeletePermissionRequestValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e DeletePermissionRequestValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e DeletePermissionRequestValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e DeletePermissionRequestValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e DeletePermissionRequestValidationError) ErrorName() string {
	return "DeletePermissionRequestValidationError"
}

// Error satisfies the builtin error interface
func (e DeletePermissionRequestValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sDeletePermissionRequest.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = DeletePermissionRequestValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = DeletePermissionRequestValidationError{}
