// Code generated by protoc-gen-validate. DO NOT EDIT.
// source: ory/keto/acl/v1alpha1/read_service.proto

package acl

import (
	"bytes"
	"errors"
	"fmt"
	"net"
	"net/mail"
	"net/url"
	"regexp"
	"sort"
	"strings"
	"time"
	"unicode/utf8"

	"google.golang.org/protobuf/types/known/anypb"
)

// ensure the imports are used
var (
	_ = bytes.MinRead
	_ = errors.New("")
	_ = fmt.Print
	_ = utf8.UTFMax
	_ = (*regexp.Regexp)(nil)
	_ = (*strings.Reader)(nil)
	_ = net.IPv4len
	_ = time.Duration(0)
	_ = (*url.URL)(nil)
	_ = (*mail.Address)(nil)
	_ = anypb.Any{}
	_ = sort.Sort
)

// Validate checks the field values on ListRelationTuplesRequest with the rules
// defined in the proto definition for this message. If any rules are
// violated, the first error encountered is returned, or nil if there are no violations.
func (m *ListRelationTuplesRequest) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on ListRelationTuplesRequest with the
// rules defined in the proto definition for this message. If any rules are
// violated, the result is a list of violation errors wrapped in
// ListRelationTuplesRequestMultiError, or nil if none found.
func (m *ListRelationTuplesRequest) ValidateAll() error {
	return m.validate(true)
}

func (m *ListRelationTuplesRequest) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	if all {
		switch v := interface{}(m.GetQuery()).(type) {
		case interface{ ValidateAll() error }:
			if err := v.ValidateAll(); err != nil {
				errors = append(errors, ListRelationTuplesRequestValidationError{
					field:  "Query",
					reason: "embedded message failed validation",
					cause:  err,
				})
			}
		case interface{ Validate() error }:
			if err := v.Validate(); err != nil {
				errors = append(errors, ListRelationTuplesRequestValidationError{
					field:  "Query",
					reason: "embedded message failed validation",
					cause:  err,
				})
			}
		}
	} else if v, ok := interface{}(m.GetQuery()).(interface{ Validate() error }); ok {
		if err := v.Validate(); err != nil {
			return ListRelationTuplesRequestValidationError{
				field:  "Query",
				reason: "embedded message failed validation",
				cause:  err,
			}
		}
	}

	if all {
		switch v := interface{}(m.GetExpandMask()).(type) {
		case interface{ ValidateAll() error }:
			if err := v.ValidateAll(); err != nil {
				errors = append(errors, ListRelationTuplesRequestValidationError{
					field:  "ExpandMask",
					reason: "embedded message failed validation",
					cause:  err,
				})
			}
		case interface{ Validate() error }:
			if err := v.Validate(); err != nil {
				errors = append(errors, ListRelationTuplesRequestValidationError{
					field:  "ExpandMask",
					reason: "embedded message failed validation",
					cause:  err,
				})
			}
		}
	} else if v, ok := interface{}(m.GetExpandMask()).(interface{ Validate() error }); ok {
		if err := v.Validate(); err != nil {
			return ListRelationTuplesRequestValidationError{
				field:  "ExpandMask",
				reason: "embedded message failed validation",
				cause:  err,
			}
		}
	}

	// no validation rules for Snaptoken

	// no validation rules for PageSize

	// no validation rules for PageToken

	if len(errors) > 0 {
		return ListRelationTuplesRequestMultiError(errors)
	}

	return nil
}

// ListRelationTuplesRequestMultiError is an error wrapping multiple validation
// errors returned by ListRelationTuplesRequest.ValidateAll() if the
// designated constraints aren't met.
type ListRelationTuplesRequestMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m ListRelationTuplesRequestMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m ListRelationTuplesRequestMultiError) AllErrors() []error { return m }

// ListRelationTuplesRequestValidationError is the validation error returned by
// ListRelationTuplesRequest.Validate if the designated constraints aren't met.
type ListRelationTuplesRequestValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e ListRelationTuplesRequestValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e ListRelationTuplesRequestValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e ListRelationTuplesRequestValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e ListRelationTuplesRequestValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e ListRelationTuplesRequestValidationError) ErrorName() string {
	return "ListRelationTuplesRequestValidationError"
}

// Error satisfies the builtin error interface
func (e ListRelationTuplesRequestValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sListRelationTuplesRequest.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = ListRelationTuplesRequestValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = ListRelationTuplesRequestValidationError{}

// Validate checks the field values on ListRelationTuplesResponse with the
// rules defined in the proto definition for this message. If any rules are
// violated, the first error encountered is returned, or nil if there are no violations.
func (m *ListRelationTuplesResponse) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on ListRelationTuplesResponse with the
// rules defined in the proto definition for this message. If any rules are
// violated, the result is a list of violation errors wrapped in
// ListRelationTuplesResponseMultiError, or nil if none found.
func (m *ListRelationTuplesResponse) ValidateAll() error {
	return m.validate(true)
}

func (m *ListRelationTuplesResponse) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	for idx, item := range m.GetRelationTuples() {
		_, _ = idx, item

		if all {
			switch v := interface{}(item).(type) {
			case interface{ ValidateAll() error }:
				if err := v.ValidateAll(); err != nil {
					errors = append(errors, ListRelationTuplesResponseValidationError{
						field:  fmt.Sprintf("RelationTuples[%v]", idx),
						reason: "embedded message failed validation",
						cause:  err,
					})
				}
			case interface{ Validate() error }:
				if err := v.Validate(); err != nil {
					errors = append(errors, ListRelationTuplesResponseValidationError{
						field:  fmt.Sprintf("RelationTuples[%v]", idx),
						reason: "embedded message failed validation",
						cause:  err,
					})
				}
			}
		} else if v, ok := interface{}(item).(interface{ Validate() error }); ok {
			if err := v.Validate(); err != nil {
				return ListRelationTuplesResponseValidationError{
					field:  fmt.Sprintf("RelationTuples[%v]", idx),
					reason: "embedded message failed validation",
					cause:  err,
				}
			}
		}

	}

	// no validation rules for NextPageToken

	if len(errors) > 0 {
		return ListRelationTuplesResponseMultiError(errors)
	}

	return nil
}

// ListRelationTuplesResponseMultiError is an error wrapping multiple
// validation errors returned by ListRelationTuplesResponse.ValidateAll() if
// the designated constraints aren't met.
type ListRelationTuplesResponseMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m ListRelationTuplesResponseMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m ListRelationTuplesResponseMultiError) AllErrors() []error { return m }

// ListRelationTuplesResponseValidationError is the validation error returned
// by ListRelationTuplesResponse.Validate if the designated constraints aren't met.
type ListRelationTuplesResponseValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e ListRelationTuplesResponseValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e ListRelationTuplesResponseValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e ListRelationTuplesResponseValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e ListRelationTuplesResponseValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e ListRelationTuplesResponseValidationError) ErrorName() string {
	return "ListRelationTuplesResponseValidationError"
}

// Error satisfies the builtin error interface
func (e ListRelationTuplesResponseValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sListRelationTuplesResponse.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = ListRelationTuplesResponseValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = ListRelationTuplesResponseValidationError{}

// Validate checks the field values on ListRelationTuplesRequest_Query with the
// rules defined in the proto definition for this message. If any rules are
// violated, the first error encountered is returned, or nil if there are no violations.
func (m *ListRelationTuplesRequest_Query) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on ListRelationTuplesRequest_Query with
// the rules defined in the proto definition for this message. If any rules
// are violated, the result is a list of violation errors wrapped in
// ListRelationTuplesRequest_QueryMultiError, or nil if none found.
func (m *ListRelationTuplesRequest_Query) ValidateAll() error {
	return m.validate(true)
}

func (m *ListRelationTuplesRequest_Query) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	// no validation rules for Namespace

	// no validation rules for Object

	// no validation rules for Relation

	if all {
		switch v := interface{}(m.GetSubject()).(type) {
		case interface{ ValidateAll() error }:
			if err := v.ValidateAll(); err != nil {
				errors = append(errors, ListRelationTuplesRequest_QueryValidationError{
					field:  "Subject",
					reason: "embedded message failed validation",
					cause:  err,
				})
			}
		case interface{ Validate() error }:
			if err := v.Validate(); err != nil {
				errors = append(errors, ListRelationTuplesRequest_QueryValidationError{
					field:  "Subject",
					reason: "embedded message failed validation",
					cause:  err,
				})
			}
		}
	} else if v, ok := interface{}(m.GetSubject()).(interface{ Validate() error }); ok {
		if err := v.Validate(); err != nil {
			return ListRelationTuplesRequest_QueryValidationError{
				field:  "Subject",
				reason: "embedded message failed validation",
				cause:  err,
			}
		}
	}

	if len(errors) > 0 {
		return ListRelationTuplesRequest_QueryMultiError(errors)
	}

	return nil
}

// ListRelationTuplesRequest_QueryMultiError is an error wrapping multiple
// validation errors returned by ListRelationTuplesRequest_Query.ValidateAll()
// if the designated constraints aren't met.
type ListRelationTuplesRequest_QueryMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m ListRelationTuplesRequest_QueryMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m ListRelationTuplesRequest_QueryMultiError) AllErrors() []error { return m }

// ListRelationTuplesRequest_QueryValidationError is the validation error
// returned by ListRelationTuplesRequest_Query.Validate if the designated
// constraints aren't met.
type ListRelationTuplesRequest_QueryValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e ListRelationTuplesRequest_QueryValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e ListRelationTuplesRequest_QueryValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e ListRelationTuplesRequest_QueryValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e ListRelationTuplesRequest_QueryValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e ListRelationTuplesRequest_QueryValidationError) ErrorName() string {
	return "ListRelationTuplesRequest_QueryValidationError"
}

// Error satisfies the builtin error interface
func (e ListRelationTuplesRequest_QueryValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sListRelationTuplesRequest_Query.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = ListRelationTuplesRequest_QueryValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = ListRelationTuplesRequest_QueryValidationError{}
