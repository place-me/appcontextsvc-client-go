module gitlab.com/place-me/appcontextsvc-client-go

go 1.19

require (
	github.com/envoyproxy/protoc-gen-validate v0.6.1
	github.com/golang/protobuf v1.5.2
	google.golang.org/genproto v0.0.0-20210831024726-fe130286e0e2
	google.golang.org/grpc v1.49.0
	google.golang.org/protobuf v1.28.1
)

require (
	gitlab.com/place-me/place-to-go/grpc v0.0.0-20220916080217-ce877437a71c // indirect
	golang.org/x/net v0.0.0-20210825183410-e898025ed96a // indirect
	golang.org/x/sys v0.0.0-20210823070655-63515b42dcdf // indirect
	golang.org/x/text v0.3.7 // indirect
)
