// Code generated by protoc-gen-validate. DO NOT EDIT.
// source: favorite_svc.proto

package appcontextsvc_client

import (
	"bytes"
	"errors"
	"fmt"
	"net"
	"net/mail"
	"net/url"
	"regexp"
	"sort"
	"strings"
	"time"
	"unicode/utf8"

	"google.golang.org/protobuf/types/known/anypb"
)

// ensure the imports are used
var (
	_ = bytes.MinRead
	_ = errors.New("")
	_ = fmt.Print
	_ = utf8.UTFMax
	_ = (*regexp.Regexp)(nil)
	_ = (*strings.Reader)(nil)
	_ = net.IPv4len
	_ = time.Duration(0)
	_ = (*url.URL)(nil)
	_ = (*mail.Address)(nil)
	_ = anypb.Any{}
	_ = sort.Sort
)

// define the regex for a UUID once up-front
var _favorite_svc_uuidPattern = regexp.MustCompile("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}$")

// Validate checks the field values on ListFavoriteResponse with the rules
// defined in the proto definition for this message. If any rules are
// violated, the first error encountered is returned, or nil if there are no violations.
func (m *ListFavoriteResponse) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on ListFavoriteResponse with the rules
// defined in the proto definition for this message. If any rules are
// violated, the result is a list of violation errors wrapped in
// ListFavoriteResponseMultiError, or nil if none found.
func (m *ListFavoriteResponse) ValidateAll() error {
	return m.validate(true)
}

func (m *ListFavoriteResponse) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	for idx, item := range m.GetFavorites() {
		_, _ = idx, item

		if all {
			switch v := interface{}(item).(type) {
			case interface{ ValidateAll() error }:
				if err := v.ValidateAll(); err != nil {
					errors = append(errors, ListFavoriteResponseValidationError{
						field:  fmt.Sprintf("Favorites[%v]", idx),
						reason: "embedded message failed validation",
						cause:  err,
					})
				}
			case interface{ Validate() error }:
				if err := v.Validate(); err != nil {
					errors = append(errors, ListFavoriteResponseValidationError{
						field:  fmt.Sprintf("Favorites[%v]", idx),
						reason: "embedded message failed validation",
						cause:  err,
					})
				}
			}
		} else if v, ok := interface{}(item).(interface{ Validate() error }); ok {
			if err := v.Validate(); err != nil {
				return ListFavoriteResponseValidationError{
					field:  fmt.Sprintf("Favorites[%v]", idx),
					reason: "embedded message failed validation",
					cause:  err,
				}
			}
		}

	}

	// no validation rules for PageNumber

	// no validation rules for PageSize

	// no validation rules for TotalCount

	if len(errors) > 0 {
		return ListFavoriteResponseMultiError(errors)
	}

	return nil
}

// ListFavoriteResponseMultiError is an error wrapping multiple validation
// errors returned by ListFavoriteResponse.ValidateAll() if the designated
// constraints aren't met.
type ListFavoriteResponseMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m ListFavoriteResponseMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m ListFavoriteResponseMultiError) AllErrors() []error { return m }

// ListFavoriteResponseValidationError is the validation error returned by
// ListFavoriteResponse.Validate if the designated constraints aren't met.
type ListFavoriteResponseValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e ListFavoriteResponseValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e ListFavoriteResponseValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e ListFavoriteResponseValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e ListFavoriteResponseValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e ListFavoriteResponseValidationError) ErrorName() string {
	return "ListFavoriteResponseValidationError"
}

// Error satisfies the builtin error interface
func (e ListFavoriteResponseValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sListFavoriteResponse.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = ListFavoriteResponseValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = ListFavoriteResponseValidationError{}

// Validate checks the field values on CreateFavoriteRequest with the rules
// defined in the proto definition for this message. If any rules are
// violated, the first error encountered is returned, or nil if there are no violations.
func (m *CreateFavoriteRequest) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on CreateFavoriteRequest with the rules
// defined in the proto definition for this message. If any rules are
// violated, the result is a list of violation errors wrapped in
// CreateFavoriteRequestMultiError, or nil if none found.
func (m *CreateFavoriteRequest) ValidateAll() error {
	return m.validate(true)
}

func (m *CreateFavoriteRequest) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	if all {
		switch v := interface{}(m.GetFavorite()).(type) {
		case interface{ ValidateAll() error }:
			if err := v.ValidateAll(); err != nil {
				errors = append(errors, CreateFavoriteRequestValidationError{
					field:  "Favorite",
					reason: "embedded message failed validation",
					cause:  err,
				})
			}
		case interface{ Validate() error }:
			if err := v.Validate(); err != nil {
				errors = append(errors, CreateFavoriteRequestValidationError{
					field:  "Favorite",
					reason: "embedded message failed validation",
					cause:  err,
				})
			}
		}
	} else if v, ok := interface{}(m.GetFavorite()).(interface{ Validate() error }); ok {
		if err := v.Validate(); err != nil {
			return CreateFavoriteRequestValidationError{
				field:  "Favorite",
				reason: "embedded message failed validation",
				cause:  err,
			}
		}
	}

	if len(errors) > 0 {
		return CreateFavoriteRequestMultiError(errors)
	}

	return nil
}

// CreateFavoriteRequestMultiError is an error wrapping multiple validation
// errors returned by CreateFavoriteRequest.ValidateAll() if the designated
// constraints aren't met.
type CreateFavoriteRequestMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m CreateFavoriteRequestMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m CreateFavoriteRequestMultiError) AllErrors() []error { return m }

// CreateFavoriteRequestValidationError is the validation error returned by
// CreateFavoriteRequest.Validate if the designated constraints aren't met.
type CreateFavoriteRequestValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e CreateFavoriteRequestValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e CreateFavoriteRequestValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e CreateFavoriteRequestValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e CreateFavoriteRequestValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e CreateFavoriteRequestValidationError) ErrorName() string {
	return "CreateFavoriteRequestValidationError"
}

// Error satisfies the builtin error interface
func (e CreateFavoriteRequestValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sCreateFavoriteRequest.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = CreateFavoriteRequestValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = CreateFavoriteRequestValidationError{}

// Validate checks the field values on DeleteFavoriteRequest with the rules
// defined in the proto definition for this message. If any rules are
// violated, the first error encountered is returned, or nil if there are no violations.
func (m *DeleteFavoriteRequest) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on DeleteFavoriteRequest with the rules
// defined in the proto definition for this message. If any rules are
// violated, the result is a list of violation errors wrapped in
// DeleteFavoriteRequestMultiError, or nil if none found.
func (m *DeleteFavoriteRequest) ValidateAll() error {
	return m.validate(true)
}

func (m *DeleteFavoriteRequest) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	if err := m._validateUuid(m.GetId()); err != nil {
		err = DeleteFavoriteRequestValidationError{
			field:  "Id",
			reason: "value must be a valid UUID",
			cause:  err,
		}
		if !all {
			return err
		}
		errors = append(errors, err)
	}

	if len(errors) > 0 {
		return DeleteFavoriteRequestMultiError(errors)
	}

	return nil
}

func (m *DeleteFavoriteRequest) _validateUuid(uuid string) error {
	if matched := _favorite_svc_uuidPattern.MatchString(uuid); !matched {
		return errors.New("invalid uuid format")
	}

	return nil
}

// DeleteFavoriteRequestMultiError is an error wrapping multiple validation
// errors returned by DeleteFavoriteRequest.ValidateAll() if the designated
// constraints aren't met.
type DeleteFavoriteRequestMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m DeleteFavoriteRequestMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m DeleteFavoriteRequestMultiError) AllErrors() []error { return m }

// DeleteFavoriteRequestValidationError is the validation error returned by
// DeleteFavoriteRequest.Validate if the designated constraints aren't met.
type DeleteFavoriteRequestValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e DeleteFavoriteRequestValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e DeleteFavoriteRequestValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e DeleteFavoriteRequestValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e DeleteFavoriteRequestValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e DeleteFavoriteRequestValidationError) ErrorName() string {
	return "DeleteFavoriteRequestValidationError"
}

// Error satisfies the builtin error interface
func (e DeleteFavoriteRequestValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sDeleteFavoriteRequest.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = DeleteFavoriteRequestValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = DeleteFavoriteRequestValidationError{}

// Validate checks the field values on GetFavoriteRequest with the rules
// defined in the proto definition for this message. If any rules are
// violated, the first error encountered is returned, or nil if there are no violations.
func (m *GetFavoriteRequest) Validate() error {
	return m.validate(false)
}

// ValidateAll checks the field values on GetFavoriteRequest with the rules
// defined in the proto definition for this message. If any rules are
// violated, the result is a list of violation errors wrapped in
// GetFavoriteRequestMultiError, or nil if none found.
func (m *GetFavoriteRequest) ValidateAll() error {
	return m.validate(true)
}

func (m *GetFavoriteRequest) validate(all bool) error {
	if m == nil {
		return nil
	}

	var errors []error

	if err := m._validateUuid(m.GetId()); err != nil {
		err = GetFavoriteRequestValidationError{
			field:  "Id",
			reason: "value must be a valid UUID",
			cause:  err,
		}
		if !all {
			return err
		}
		errors = append(errors, err)
	}

	if len(errors) > 0 {
		return GetFavoriteRequestMultiError(errors)
	}

	return nil
}

func (m *GetFavoriteRequest) _validateUuid(uuid string) error {
	if matched := _favorite_svc_uuidPattern.MatchString(uuid); !matched {
		return errors.New("invalid uuid format")
	}

	return nil
}

// GetFavoriteRequestMultiError is an error wrapping multiple validation errors
// returned by GetFavoriteRequest.ValidateAll() if the designated constraints
// aren't met.
type GetFavoriteRequestMultiError []error

// Error returns a concatenation of all the error messages it wraps.
func (m GetFavoriteRequestMultiError) Error() string {
	var msgs []string
	for _, err := range m {
		msgs = append(msgs, err.Error())
	}
	return strings.Join(msgs, "; ")
}

// AllErrors returns a list of validation violation errors.
func (m GetFavoriteRequestMultiError) AllErrors() []error { return m }

// GetFavoriteRequestValidationError is the validation error returned by
// GetFavoriteRequest.Validate if the designated constraints aren't met.
type GetFavoriteRequestValidationError struct {
	field  string
	reason string
	cause  error
	key    bool
}

// Field function returns field value.
func (e GetFavoriteRequestValidationError) Field() string { return e.field }

// Reason function returns reason value.
func (e GetFavoriteRequestValidationError) Reason() string { return e.reason }

// Cause function returns cause value.
func (e GetFavoriteRequestValidationError) Cause() error { return e.cause }

// Key function returns key value.
func (e GetFavoriteRequestValidationError) Key() bool { return e.key }

// ErrorName returns error name.
func (e GetFavoriteRequestValidationError) ErrorName() string {
	return "GetFavoriteRequestValidationError"
}

// Error satisfies the builtin error interface
func (e GetFavoriteRequestValidationError) Error() string {
	cause := ""
	if e.cause != nil {
		cause = fmt.Sprintf(" | caused by: %v", e.cause)
	}

	key := ""
	if e.key {
		key = "key for "
	}

	return fmt.Sprintf(
		"invalid %sGetFavoriteRequest.%s: %s%s",
		key,
		e.field,
		e.reason,
		cause)
}

var _ error = GetFavoriteRequestValidationError{}

var _ interface {
	Field() string
	Reason() string
	Key() bool
	Cause() error
	ErrorName() string
} = GetFavoriteRequestValidationError{}
